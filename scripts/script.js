// OPTION 1
let items = ["hello", "world", 23, "23", null];
const filterBy = (arr, type) => arr.filter((item) => typeof item !== type);
console.log(filterBy(items, "string"));


// OPTION 2
// function filterBy(arr, mytype) {
//   let result = [];
//   for (let i = 0; i < arr.length; i++) {
//     if (typeof arr[i] !== mytype) {
//       result.push(arr[i]);
//     }
//   }
//   return result;
// }
// console.log(filterBy(["hello", "world", 23, "23", null], "string"));


